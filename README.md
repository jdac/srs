Shaw Reporting Service WP Site
============================

Custom WordPress theme and plugin development for the Shaw Reporting Services website.

* Theme JCS Services
* Version 1.0

### How do I get set up? ###
* WordPress >= 5.8.2
* PHP ~=7.4

### Required WordPress Plugins ###
* Advanced Custom Fields - Delicious Brains
* Custom Post Type UI - WebDevStudios

### Recommended WordPress Plugins ###
* Disable Gutenberg - Jeff Star
* Post Types Order - Nsp Code
* Regenerate Thumbnails - Alex Mills

### Dependencies ###
* FontAwesome 5.15
* jquery Fancybox 3.5.7
* Vegas Slider CSS/JS Library 2.5.1
* OnScreen Javascript Library v3
* Parallax JavaScript Library 1.5.0
* Waypoints JavaScript Library 4.0.1
* Conditionizr 4.3.0
* Modernizr 2.7.1

## Contact Info ###
* DriveJCS (https://jdacsolutions.com)
